# gitlab-tutorial

## clone repository

1. go to the folder where you want to clone the repository
2. copy the path of the folder
3. open command prompt and type command cd <path> - copied in #2
4. type `git clone <url of gitlab repo>`
5. cd into the newly created folder
6. open this folder in VS code

## making changes and pushing it to gitlab

1. after making changes in your files
2. go to command line and type `git add .`
3. type `git status` and all your changes should be in green
4. type `git commit -m "commit message"`
5. when it is successful type `git push origin master`


## working with branches in gitlab

1. to create a new branch type `git checkout -b <branch-name>`
2. make the changes in the branch and commit the changes to the branch
3. push the branch to gitlab by using command `git push -set--upstream origin <branch-name>`
4. go to gitlab.com/project-name
5. create merge request from `<branch-name>` to `master`
6. once the merge is done (assuming no conflicts) you should see all the changes in the branch merged into master

